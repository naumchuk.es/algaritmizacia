package sun

/**
 * Целочисленое сложение
 */
fun add(a1: Int, a2: Int) = a1 + a2
