package com.example.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import sun.add

class Tests {

    @Test
    fun `2 + 2 = 4`() {
        assertEquals(4, add(2, 2), "2 + 2 should equal 4")
    }
}
