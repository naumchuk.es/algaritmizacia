package com.example.tests

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class sTests {

    @Test
    fun `2 + 2 = 4`() {
        assertEquals(4, 2 + 2, "2 + 2 should equal 4")
    }
}
